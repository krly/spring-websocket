package com.carly.webchaussettes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebchaussettesApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebchaussettesApplication.class, args);
	}

}

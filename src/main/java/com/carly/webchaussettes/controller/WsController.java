package com.carly.webchaussettes.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WsController {

    /**
     * Retourne le nom du template Thymeleaf qui sera envoyé comme réponse
     * @return
     */
    @RequestMapping("/websocket")
    public String getWebSocket() {
        return "ws-broadcast";
    }
}

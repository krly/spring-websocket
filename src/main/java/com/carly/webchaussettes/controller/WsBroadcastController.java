package com.carly.webchaussettes.controller;

import com.carly.webchaussettes.model.ChatMessage;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WsBroadcastController {

    @GetMapping("/stomp-broadcast")
    public String getWsBroadcast() {
        return "stomp-broadcast";
    }

    /**
     * Pour mapper les messages envoyés sur /broadcast (@MessageMapping) et envoyer un objet Message en retour à /messages (@SendTo)
     * le Message possède un header + body
     * @param chatMessage
     * @return
     */
    @MessageMapping("/broadcast")
    @SendTo("/topic/messages")
    public ChatMessage send(ChatMessage chatMessage) {
        return new ChatMessage(chatMessage.getFrom(), chatMessage.getText(), "ALL");
    }
}
